var util = require('util')

var bleno = require('bleno')
var BlenoPrimaryService = bleno.PrimaryService

var IndicateCharacteristic = require('./indicate-characteristic')
var WriteCharacteristic = require('./write-characteristic')

function ClipsService (fakeClips) {
  ClipsService.super_.call(this, {
    uuid: '00000003000310008000001a11000100',
    characteristics: [
      new WriteCharacteristic(fakeClips),
      new IndicateCharacteristic(fakeClips)
    ]
  })
}

util.inherits(ClipsService, BlenoPrimaryService)

module.exports = ClipsService
