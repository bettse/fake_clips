var bleno = require('bleno')
var ClipsService = require('./clips-service')
var debug = require('debug')('FakeClips:ble')
var Device = require('./device')

// Has the service UUID backwards
const scanData = Buffer.from('020a041107000100111a0000800010030003000000', 'hex')
const advertisementData = Buffer.from('020106' + '04ffe00001' + '080947432d33343732', 'hex')

class Ble {
  constructor () {
    this.device = new Device()
    this.stateChange = this.stateChange.bind(this);
    this.advertisingStart = this.advertisingStart.bind(this);
    this.accept = this.accept.bind(this);
    this.disconnect = this.disconnect.bind(this);

    bleno.on('stateChange', this.stateChange)
    bleno.on('advertisingStart', this.advertisingStart)
    bleno.on('accept', this.accept)
    bleno.on('disconnect', this.disconnect)
  }

  stateChange (state) {
    debug('stateChange:', state)

    if (state === 'poweredOn') {
      bleno.startAdvertisingWithEIRData(advertisementData, scanData)
    } else {
      bleno.stopAdvertising()
    }
  }

  advertisingStart (error) {
    if (error) {
      console.log('advertisingStart error:', error)
      return
    }
    debug('advertisingStart')
    const clipsService = new ClipsService(this.device)
    bleno.setServices([clipsService], this.onError)
  }

  accept (clientAddress) {
    debug('accept', clientAddress)
    bleno.stopAdvertising()
  }

  disconnect (clientAddress) {
    debug('disconnect', clientAddress)
    setTimeout(() => this.stateChange('poweredOn'), 100)
  }

  onError (error) {
    if (error) {
      debug('bleno error', error)
      return
    }
  }
}

module.exports = Ble
