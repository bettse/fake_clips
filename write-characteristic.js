var util = require('util')
var bleno = require('bleno')
var debug = require('debug')('FakeClips:WriteCharacteristic')
var BlenoCharacteristic = bleno.Characteristic

function WriteCharacteristic (fakeClips) {
  WriteCharacteristic.super_.call(this, {
    uuid: '00010001000310008000001A11000100',
    properties: ['read', 'write']
  })
  this.fakeClips = fakeClips
}

util.inherits(WriteCharacteristic, BlenoCharacteristic)

WriteCharacteristic.prototype.onWriteRequest = function (data, offset, withoutResponse, callback) {
  if (offset) {
    console.log('onWriteRequest with offset unsupported')
    callback(this.RESULT_ATTR_NOT_LONG)
  } else {
    this.fakeClips.newChunk(data)
    callback(this.RESULT_SUCCESS)
  }
}

module.exports = WriteCharacteristic
