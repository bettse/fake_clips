var util = require('util')
var bleno = require('bleno')
var debug = require('debug')('FakeClips:IndicateCharacteristic')

var BlenoCharacteristic = bleno.Characteristic

function IndicateCharacteristic (fakeClips) {
  IndicateCharacteristic.super_.call(this, {
    uuid: '80010001000310008000001A11000100',
    properties: ['read', 'indicate']
  })
  this.fakeClips = fakeClips
}

util.inherits(IndicateCharacteristic, BlenoCharacteristic)

IndicateCharacteristic.prototype.onSubscribe = function (maxValueSize, updateValueCallback) {
  debug('IndicateCharacteristic subscribe', maxValueSize)
  this.fakeClips.setUpdateCallback(updateValueCallback, maxValueSize)
}

IndicateCharacteristic.prototype.onUnsubscribe = function () {
  debug('IndicateCharacteristic unsubscribe')
  this.fakeClips.setUpdateCallback(console.log)
}

IndicateCharacteristic.prototype.onIndicate = function () {
  debug('IndicateCharacteristic onIndicate')
  this.fakeClips.receiveConfimation()
}

module.exports = IndicateCharacteristic
