const debug = require('debug')('FakeClips:device')
const protobuf = require('protobufjs')
const root = protobuf.loadSync('fake_clips.proto')
const crypto = require('crypto')
const ecdh = crypto.createECDH('prime256v1')

const requestClass = root.lookupType('fake_clips.Request')
const responseClass = root.lookupType('fake_clips.Response')

class Device {
  constructor() {
    this.packetId = 0
    this.public_key = ecdh.generateKeys()
  }

  newChunk (chunk) {
    debug('newChunk', chunk.toString('hex'))
    const length = chunk[0]
    const value = chunk.slice(1)
    const request = requestClass.decode(value)
    debug(request)

    if (request.publicQuery) {
      debug(request.publicQuery.timeUtc.toString())
      this.sendResponse({
        packetId: this.packetId++,
        publicQuery: {
          a: 1,
          hardwareRevision: 5,
          firmwareVersion1: 1,
          firmwareVersion2: 6,
          firmwareVersion3: 18061913,
          firmwareVersion4: 202690470,
          g: 26,
          goldFirmwareVersion1: 1,
          goldFirmwareVersion2: 3,
          goldFirmwareVersion3: 5,
          goldFirmwareVersion4: 179853975,
          storageCapacity: 14283636736
        }
      })
    } else if (request.flashIdentifyLeds) {
      this.sendResponse({
        packetId: this.packetId++,
        flashIdentifyLeds: {
          a: 1
        }
      })
    } else if (request.initiatePairing) {
      const { b } = request.initiatePairing
      this.sharedSecret = ecdh.computeSecret(Buffer.concat([Buffer.from([0x04]), b]))
      debug({sharedSecret: this.sharedSecret.toString('hex')})

      this.sendResponse({
        packetId: this.packetId++,
        initiatePairing: {
          a: 1,
          b: 1,
          c: this.public_key.slice(1)
        }
      })

    } else if (request.initiateSecureConnection) {
      this.sendResponse({
        packetId: this.packetId++,
        initiateSecureConnection: {
          a: 1,
          b: crypto.randomBytes(16),
          c: crypto.randomBytes(32)
        }
      })
    }
  }

  sendResponse(obj) {
    const response = responseClass.create(obj)
    const bytes = responseClass.encode(response).finish()
    const header = Buffer.from([bytes.length])
    debug(response)
    this.send(Buffer.concat([header, bytes]))
  }

  send(chunk) {
    debug('=>', chunk.toString('hex'))
    this.updateCallback(chunk)
  }

  setUpdateCallback (cb, maxValueSize) {
    this.updateCallback = cb
    this.maxValueSize = maxValueSize
  }

  receiveConfimation () {
    debug('receiveConfimation')
  }
}

module.exports = Device
